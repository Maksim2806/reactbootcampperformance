import { useEffect, useState, useCallback, memo, useMemo } from "react";
import "./App.css";

const Button = memo((props) => {
  console.count("render button");
  return <button {...props} style={{ backgroundColor: "lightgray" }} />;
})

const ListItem = memo(({ children }) => {
  console.count("render list item");
  return (
    <li>
      {children}
      <label style={{ fontSize: "smaller" }}>
        <input type="checkbox" />
        Add to cart
      </label>
    </li>
  );
})

function App() {
  const [searchString, setSearchString] = useState("");
  const [isSortingDesc, setSortingDesc] = useState(false);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    console.count("render fetch");
    fetch("https://reqres.in/api/products")
      .then((response) => response.json())
      .then((json) =>
        setProducts(json.data)
      ).catch((error) => console.error(error));
  }, []);

  const productList = useMemo(() => {
    const preparedProducts = products.filter((item) => item.name.includes(searchString))
    .sort((a, z) =>
      isSortingDesc
        ? z.name.localeCompare(a.name)
        : a.name.localeCompare(z.name)
    )
    return preparedProducts.map(({name}) => {
      return <ListItem key={name}>{name}</ListItem>;
    })
  }, [products, searchString, isSortingDesc])

  console.count("render app");

  const toggleSorting = useCallback(() => setSortingDesc((value) => !value), [])

  const handleSerchStringChange = useCallback((e) => setSearchString(e.target.value), [])

  return (
    <div className="App">
      <input
        type="search"
        value={searchString}
        onChange={handleSerchStringChange}
      />
      <Button onClick={toggleSorting}>
        Change sort direction
      </Button>
      <ul>
        {productList}
      </ul>
    </div>
  );
}

export default App;